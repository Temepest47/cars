﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamera : MonoBehaviour {

	
	public void RotireSpreTinta()
	{
		Vector3 directieDeRotire = obiectDeUrmarit.position - transform.position;
		Quaternion _rot = Quaternion.LookRotation(directieDeRotire, Vector3.up);
		transform.rotation = Quaternion.Lerp(transform.rotation, _rot, vitezaDeRotare * Time.deltaTime);
	}

	public void MergiLaTinta()
	{
		Vector3 _targetPos = obiectDeUrmarit.position + 
							 obiectDeUrmarit.forward * offset.z + 
							 obiectDeUrmarit.right * offset.x + 
							 obiectDeUrmarit.up * offset.y;
		transform.position = Vector3.Lerp(transform.position, _targetPos, vitezaUrmarire * Time.deltaTime);
	}

	private void FixedUpdate()
	{
		RotireSpreTinta();
		MergiLaTinta();
	}

	public Transform obiectDeUrmarit;
	public Vector3 offset;
	public float vitezaUrmarire = 10;
	public float vitezaDeRotare = 10;
}
