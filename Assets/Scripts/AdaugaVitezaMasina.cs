﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdaugaVitezaMasina : MonoBehaviour
{
    public float deltaAditieViteza = 5.0F;

    private float urmatoareaAditieViteza = 5.0F;
    private float timp = 0.0F;
    public float aditieViteza = 20;
    void Update()
    {
        timp += Time.deltaTime;
    }
    void OnTriggerEnter(Collider col)
    {
        if(col.CompareTag("Jucator"))
        {
            if(timp> urmatoareaAditieViteza)
            {
                urmatoareaAditieViteza = deltaAditieViteza;
                col.GetComponent<ControlMasina>().fortaMotor += aditieViteza;
                timp = 0.0F;
            }
        }
    }
}
