﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UrmaresteDrum : MonoBehaviour
{
    public List<Transform> steaguri;
    public float vitezaDeUrmarire = 10;
    public float vitezaDeRotare = 10;

    void Update()
    {
        MergiLaTinta();
    }
    void RotireLaTinta(Transform obiectDeUrmarit)
    {
        Vector3 directie = new Vector3
        (
            obiectDeUrmarit.position.x - transform.position.x,
            0,
            obiectDeUrmarit.position.z - transform.position.z
        );
        Vector3 rotatie = Vector3.RotateTowards(transform.forward, directie, vitezaDeRotare * Time.deltaTime, 0.0f);
        transform.rotation = Quaternion.LookRotation(rotatie);
    }
    void MergiLaTinta()
    {
        if(steaguri.Count > 0)
        {
            Transform steagCurent = steaguri[0];
            if((transform.position.x == steagCurent.position.x) && (transform.position.z == steagCurent.position.z))
            {
                steaguri.Remove(steagCurent);
                steaguri.Add(steagCurent);
                steagCurent = steaguri[0];
            }
            RotireLaTinta(steagCurent);
            transform.position = new Vector3(
                Mathf.MoveTowards(transform.position.x, steagCurent.position.x, vitezaDeUrmarire * Time.deltaTime),
                steaguri[steaguri.Count - 1].position.y,
                Mathf.MoveTowards(transform.position.z, steagCurent.position.z, vitezaDeUrmarire * Time.deltaTime));
        }
    }
}
