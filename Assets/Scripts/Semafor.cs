﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Semafor : MonoBehaviour
{
    public Statusuri status;
    public GameObject rosu;
    public GameObject galben;
    public GameObject verde;
    bool omoaraJucator = false;
    void Start()
    {
        StartCoroutine(SchimbaCulori());
    }

    IEnumerator SchimbaCulori()
    {
        while(true)
        {
            omoaraJucator = false;
            rosu.SetActive(false);
            verde.SetActive(true);
            yield return new WaitForSeconds(15);
            verde.SetActive(false);
            galben.SetActive(true);
            yield return new WaitForSeconds(2);
            omoaraJucator = true;
            galben.SetActive(false);
            rosu.SetActive(true);
            yield return new WaitForSeconds(7);
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        OmoaraJucator(collision);
    }
    void OnTriggerStay(Collider collision)
    {
        OmoaraJucator(collision);
    }
    void OmoaraJucator(Collider collision)
    {
        if(omoaraJucator)
        {
            if(collision.gameObject.CompareTag("Jucator"))
            {
               status.ActualizeazaPanou(true, true, "Ai murit \n Ai traversat pe culoarea rosu \n Apasa R sa reincepi de la capat", 0);
            }
        }
    }
}
