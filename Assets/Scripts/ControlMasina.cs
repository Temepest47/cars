﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMasina : MonoBehaviour {

	public void PreiaInput()
	{
		InputOrizonta = Input.GetAxis("Horizontal");
		InputVertical = Input.GetAxis("Vertical");
	}

	private void Rotire()
	{
		unghiDeRotire = unghiMaximDeRotire * InputOrizonta;
		roataSoferFata.steerAngle = unghiDeRotire;
		roataPasagerFata.steerAngle = unghiDeRotire;
	}

	private void Accelerare()
	{
		roataSoferFata.motorTorque = InputVertical * fortaMotor;
		roataPasagerFata.motorTorque = InputVertical * fortaMotor;
	}

	private void ActualizeazaPositiileRotilor()
	{
		ActualizeazaPozitiaRotii(roataSoferFata, transformSoferFata);
		ActualizeazaPozitiaRotii(roataPasagerFata, transformPasagerFata);
		ActualizeazaPozitiaRotii(roataSoferSpate, transformSoferSpate);
		ActualizeazaPozitiaRotii(roataPasagerSpate, transfomrPasagerSpate);
	}

	private void ActualizeazaPozitiaRotii(WheelCollider _collider, Transform _transform)
	{
		Vector3 _pos = _transform.position;
		Quaternion _quat = _transform.rotation;

		_collider.GetWorldPose(out _pos, out _quat);

		_transform.position = _pos;
		_transform.rotation = _quat;
	}

	private void FixedUpdate()
	{
		PreiaInput();
		Rotire();
		Accelerare();
		ActualizeazaPositiileRotilor();
	}

	private float InputOrizonta;
	private float InputVertical;
	private float unghiDeRotire;

	public WheelCollider roataSoferFata, roataPasagerFata;
	public WheelCollider roataSoferSpate, roataPasagerSpate;
	public Transform transformSoferFata, transformPasagerFata;
	public Transform transformSoferSpate, transfomrPasagerSpate;
	public float unghiMaximDeRotire = 30;
	public float fortaMotor = 50;
}
