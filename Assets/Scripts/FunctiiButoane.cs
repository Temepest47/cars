﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FunctiiButoane : MonoBehaviour
{
    public void IncarcaScena(int index)
    {
        SceneManager.LoadScene(index);
    }
    public void IesiAplicatie()
    {
        //nu merge in modul editor
        Application.Quit();
    }
}
