﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Statusuri : MonoBehaviour
{
    public bool sfarsitJoc = false;
    public bool pauzaJoc = false;
    public GameObject panou;
    public GameObject text;
    public GameObject butonMeniu;
    public GameObject butonIesire;
    public Text timer;
    private float timpInJoc;
    void Update()
    {
        timpInJoc += Time.deltaTime;
        timer.text = ((int)timpInJoc / 3600 % 3600)   + ":" + ((int)timpInJoc / 60 % 60)  + ":" + ((int)timpInJoc % 60) ;
    }
    public void ActualizeazaPanou(bool stadiuPanou, bool stadiuText, string text, float timeScale, bool meniu = false, bool iesire = false)
    {
        panou.SetActive(stadiuPanou);
        this.text.SetActive(stadiuText);
        this.text.GetComponent<Text>().text = text;
        Time.timeScale = timeScale;
        butonMeniu.SetActive(meniu);
        butonIesire.SetActive(iesire);
    }
}
