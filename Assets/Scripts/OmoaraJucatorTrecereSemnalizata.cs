﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OmoaraJucatorTrecereSemnalizata : MonoBehaviour
{
    public Statusuri status;
    public bool omoara = false;
    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.CompareTag("Pieton"))
        {
            omoara = true;
        }
        OmoaraJucator(col);
    }
    void OnTriggerStay(Collider col)
    {
        OmoaraJucator(col);
    }
    void OmoaraJucator(Collider col)
    {
        if(omoara)
        {
            if(col.gameObject.CompareTag("Jucator"))
            {
                status.sfarsitJoc = true;
                status.ActualizeazaPanou(true, true, "Ai murit \n Nu ai acordat permisiune pietonului \n Apasa R sa reincepi de la capat", 0);
            }
        }
    }
    void OnTriggerExit(Collider col)
    {
        if(col.gameObject.CompareTag("Pieton"))
        {
            omoara = false;
        }
    }
}
