﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SemnStop : MonoBehaviour
{
    public Statusuri status;
    private bool omoaraLaIesire = true;
    void OnTriggerStay(Collider collider)
    {
        if(collider.gameObject.CompareTag("Jucator"))
        {
            if(collider.gameObject.GetComponent<ControlMasina>()?.roataSoferFata.rpm < 1 &&
            collider.gameObject.GetComponent<ControlMasina>()?.roataSoferFata.rpm < -1)
            {
                omoaraLaIesire = false;
            }
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if(omoaraLaIesire)
        {
            if(collider.gameObject.CompareTag("Jucator"))
            {
                status.ActualizeazaPanou(true, true, "Ai murit \n Nu ai oprit la semnul Stop \n Apasa R sa reincepi de la capat", 0);
                omoaraLaIesire = true;
            }
        }else
        {
            if(collider.gameObject.CompareTag("Jucator"))
            { 
                omoaraLaIesire = true;
            }
        }
    }
}
