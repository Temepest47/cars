﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauzaJoc : MonoBehaviour
{
    public Statusuri status;
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
        {
            status.pauzaJoc = !status.pauzaJoc;
            float timeScale = status.pauzaJoc == true ? 0.0f : 1.0f;
            status.ActualizeazaPanou(status.pauzaJoc, status.pauzaJoc,
            "Ai pus pauza \n apasa P sau Esc sa repornesti jocul sau R sa resetezi", timeScale, status.pauzaJoc, status.pauzaJoc);
        }
    }
}
