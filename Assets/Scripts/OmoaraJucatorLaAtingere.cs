﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OmoaraJucatorLaAtingere : MonoBehaviour
{
    public Statusuri status;
    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.CompareTag("Jucator"))
        {
            status.sfarsitJoc = true;
            status.ActualizeazaPanou(true, true, "Ai murit \n Ai mers intr-o zona nepermisa \n Apasa R sa reincepi de la capat", 0);
        }
    }
}
